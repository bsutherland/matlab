# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

All MATLAB code

### How do I get set up? ###

This is a Library for MATLAB functions.  Download those functions needed for simulation in the MATLAB environment into your chosen MATLAB working directory.


### Contribution guidelines ###

Please add appropriate comments within the code to explain how to properly support and operate the function.  Include your name and date of origination, and a notation of Dash Networks
as the owner.

### Who do I talk to? ###

* Bob Sutherland
* Don Peck